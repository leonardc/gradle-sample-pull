# gradle-getting-started

A barebones Gradle app, which can easily be deployed to Orchard.

## Documentation

For more information about using Java on Orchard see: [Java Quickstart Guide](http://docs.orchard.apple.com/Guides/Java)
